package spring_introduction.java_cod_annotatuon;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component("myPerson1")
public class Person1 {

    private Bird bird;

    private int age;

    private String name;

    @Autowired
    public Person1(@Qualifier("myDuck") Bird bird) {
        this.bird = bird;
    }

    public Bird getBird() {
        return bird;
    }

    public void setBird(Bird bird) {
        this.bird = bird;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void helloMyBird() {
        System.out.println("Hello my bird!");
        bird.say();
    }
}
