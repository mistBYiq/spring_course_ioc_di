package spring_introduction.java_cod_annotatuon;

import org.springframework.stereotype.Component;

@Component("myDuck")
public class Duck implements Bird {

    public Duck() {
    }

    @Override
    public void say() {
        System.out.println("Krya krya");
    }
}
