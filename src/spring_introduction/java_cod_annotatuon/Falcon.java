package spring_introduction.java_cod_annotatuon;

import org.springframework.stereotype.Component;

@Component("myFalcon")
public class Falcon implements Bird {

    public Falcon() {
    }

    @Override
    public void say() {
        System.out.println("Piy piy");
    }
}
