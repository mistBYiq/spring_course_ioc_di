package spring_introduction.java_cod_annotatuon;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("spring_introduction.java_cod_annotatuon")
public class MyConfigSpringContext {
}
