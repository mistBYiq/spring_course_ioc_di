package spring_introduction.java_cod_annotatuon;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class TestJavaConfig {
    public static void main(String[] args) {

        AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(MyConfigSpringContext.class);

        Person1 person = context.getBean("myPerson1", Person1.class);
        person.helloMyBird();

        context.close();
    }
}
