package spring_introduction.java_cod_annotation2;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Test {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(MyConfig.class);

//        Bird falcon = context.getBean("falconBean", Falcon.class);
//        falcon.say();
        Person3 person = context.getBean("personBean", Person3.class);
        person.helloMyBird();

        context.close();
    }
}
