package spring_introduction.java_cod_annotation2;

public class Person3 {
    private Bird bird;

    private String name;

    private int age;

    public Person3(Bird bird) {
        this.bird = bird;
    }

    public Bird getBird() {
        return bird;
    }

    public void setBird(Bird bird) {
        this.bird = bird;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void helloMyBird() {
        System.out.println("Hello my Bird");
        bird.say();
    }
}
