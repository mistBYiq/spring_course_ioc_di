package spring_introduction.java_cod_annotation2;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MyConfig {

    @Bean
    public Bird falconBean() {
        return new Falcon();
    }

    @Bean
    public Person3 personBean() {
        return new Person3(falconBean());
    }
}
