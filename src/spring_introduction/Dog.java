package spring_introduction;

import org.springframework.stereotype.Component;

@Component
public class Dog implements Pet {
//
//    private String name;

    public Dog() {
        System.out.println("Dog bean is created");
    }

//    public String getName() {
//        return name;
//    }
//
//    public void setName(String name) {
//        this.name = name;
//    }

    @Override
    public void say() {
        System.out.println("Bow-wow!");
    }
/*
    методы init and destroy
    - у данных методов access modifier может быть любым
    - у данных методов return type может быть любым. Но из-за того, что возвращаемое значение мы никак не можем использовать,
    чаще всего используют return type - void
    - называться методы могут как угодно. главное правитьно указать в конфиге спринг контекста в бине  init-method="..."
    destroy-method="..."
    - в данных методах не должно быть параметров. так как методы вызываюся автоматически , спринг не будет знать какие параметры подставлять

    если scope="singleton" и создается несколько myDog, yourDog... , то init метод будет запускаться один раз,
    так как будет создан только один объект, но когда закроется контекст - один раз вызовестся метод destroy

    если scope="prototype" и создается несколько myDog, yourDog..., то
    - init-method будет вызываться для КАЖДОГО нового экземпляра бина,
    - destroy method для этого бина создаваться не будет
    - программисту необходимо самостоятельно писать код для закрытия/освобождения ресурсов, которые были использованны в бине
 */
    private void init() {
        System.out.println("Class Dog: method init");
    }

    protected void destroy() {
        System.out.println("Class Dog: method destroy");
    }
}