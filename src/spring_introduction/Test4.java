package spring_introduction;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test4 {
    public static void main(String[] args) {
        // создаем контейнер бинов(он же апликейшен контекст)
        // с указанием файла конфигурации для контейнера(апликейшен контекста)
        // в котором уже описанны все бины, которыми будет управлять апликейшен контекст
        ClassPathXmlApplicationContext context =
                new ClassPathXmlApplicationContext("springApplicationContext.xml");

//        Pet pet = context.getBean("myPet", Pet.class);
//        Pet pet = new Dog();

        // после изменений в файле конфигурации(внедрение зависимости бина Пет через !!!сеттер(внезапно) Персон),
        // все тоже самое - спринг делает все настройки под капотом без нас, главное правильно написать конфигурацию
        Person person = context.getBean("myPerson", Person.class);
        person.callYourPet();

        context.close(); // не забываем закрывать контекст
    }
}
