package spring_introduction;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestConfigWithAnnotation1 {
    public static void main(String[] args) {
        ClassPathXmlApplicationContext context =
                new ClassPathXmlApplicationContext("applicationContext3.xml");

       /* получаем свой синглтон бин. если в классе Кэт не прописывать id "myCat" в аннотации Components,
         то спринг сам создаст ид по умолчанию = "cat"
         -------------------
            примеры id по-умолчанию:

            @Component
            class Cat {} -> id = "cat"

            @Component
            class BestFriends {} -> id = "bestFriends"

            @Component
            class SQLTests {} - id = "SQLTests"
         ------------------
         и строка ниже имела бы вид
         Cat cat = context.getBean("cat", Cat.class);
        */
        Cat cat = context.getBean("catBean", Cat.class);
        cat.say();

        context.close();

    }
}
