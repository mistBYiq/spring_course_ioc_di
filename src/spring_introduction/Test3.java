package spring_introduction;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test3 {
    public static void main(String[] args) {
        // создаем Application Context, и указываем конфигурационый файл для спринг контекста
        ClassPathXmlApplicationContext context =
                new ClassPathXmlApplicationContext("springApplicationContext.xml");

        Pet pet = context.getBean("myPet", Pet.class);
        pet.say();

        // не забыть закрыть контекст
        context.close();
    }
}
