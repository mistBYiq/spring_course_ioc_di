package spring_introduction;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestBeanScope {
    public static void main(String[] args) {
        ClassPathXmlApplicationContext context =
                new ClassPathXmlApplicationContext("applicationContext2.xml");

        Dog myDog = context.getBean("myPet", Dog.class);
//        myDog.setName("Belka");
        Dog yourDog = context.getBean("myPet", Dog.class);
//        yourDog.setName("Strelka");
//
//        System.out.println(myDog.getName());
//        System.out.println(yourDog.getName());


        System.out.println("Переменные ссылаются на один и тот же объект? " + (myDog == yourDog));
        System.out.println(myDog);
        System.out.println(yourDog);

        context.close();

    }

    //Dog bean is created
    //Dog bean is created
    //Переменные ссылаются на один и тот же объект? false
    //spring_introduction.Dog@55ca8de8
    //spring_introduction.Dog@2c34f934

}
