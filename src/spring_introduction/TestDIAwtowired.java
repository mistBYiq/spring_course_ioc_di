package spring_introduction;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestDIAwtowired {
    public static void main(String[] args) {
        ClassPathXmlApplicationContext context =
                new ClassPathXmlApplicationContext("applicationContext3.xml");
/* @ Autowired
Для внедренния зависимости через аннотации используется аннотация @Autowired
Типы автовайрингов или где мы можем их использовать данный DI:
    - конструктор
    - сеттер
    - поле
Процесс внедрения зависимостей при использовании @Autowired :
    1. Сканирование пакета указанного в конфигурационном файле, поиск классов с @Component
    2. При наличае @Autowired начинается поиск подходящего по типу бина
        далее:
        - если находится один подходящий бин, происходит внедрение зависимости
        - если 0 , выбрасывается исключение
        - если >1 , выбрасывается исключение
*/

        Person person = context.getBean("personBean", Person.class);
        person.callYourPet();

        context.close();

    }
}
