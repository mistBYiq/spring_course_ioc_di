package spring_introduction;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestBeanInitDestroy {
    // Жизненый цикл бина
    // Запуск приложения -> Начало работы Sping Conteiner -> Создание бина -> DI - внедряются зависимости
    // -> init method(если он есть) -> Бин готов к использованию -> Использование нами бина
    // -> конец работы Sping Conteiner -> destroy method -> остановка приложения
    //
    // Где используется
    // Чаще всего init method используется для открытия или настройки каких-либо ресурсов, напримербаз данных, стримов и т.д.
    // destroy method для их закрытия

    public static void main(String[] args) {
        ClassPathXmlApplicationContext context =
                new ClassPathXmlApplicationContext("applicationContext2.xml");

        Dog myDog = context.getBean("myPet", Dog.class);
        myDog.say();

        Dog yourDog = context.getBean("myPet", Dog.class);
        myDog.say();

        context.close();
    }
}
