package spring_introduction;

public class Test2 {
    public static void main(String[] args) {
        Pet pet1 = new Dog();
        pet1.say();

        Pet pet2 = new Cat();
        pet2.say();
    }
}
