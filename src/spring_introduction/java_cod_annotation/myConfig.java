package spring_introduction.java_cod_annotation;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;

@Configuration
@PropertySource("classpath:myApp.properties")
public class myConfig {

    @Bean
    @Scope("prototype")
    public Bird falconBean() {
        return new Falcon();
    }

    @Bean
    @Scope("singleton")
    public Bird duckBean() {
        return new Duck();
    }

    @Bean
    public Person personBean() {
        return new Person(duckBean());
    }

}
