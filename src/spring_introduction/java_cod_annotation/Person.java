package spring_introduction.java_cod_annotation;

import org.springframework.beans.factory.annotation.Value;

public class Person {
    private Bird bird;

    @Value("${person.surname}")
    private String name;

    @Value("${person.age}")
    private int age;

    public Person(Bird bird) {
        this.bird = bird;
    }

    public Bird getBird() {
        return bird;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void helloMyBird() {
        System.out.println("Hello my Bird");
        bird.say();
    }
}
