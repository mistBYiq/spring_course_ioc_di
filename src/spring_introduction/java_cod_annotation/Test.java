package spring_introduction.java_cod_annotation;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Test {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(myConfig.class);

        Person person = context.getBean("personBean", Person.class);
        person.helloMyBird();
        System.out.println(person.getName());
        System.out.println(person.getAge());

        context.close();
    }
}
