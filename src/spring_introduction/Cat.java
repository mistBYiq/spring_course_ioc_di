package spring_introduction;

import org.springframework.stereotype.Component;

/* помечаем нужный нам класс @Component, и мы можем указать для него id например catBean(указываем ид для лучшей читаемости кода) */
@Component("catBean")
public class Cat implements Pet {

    public Cat() {
        System.out.println("Cat bean is created");
    }

    @Override
    public void say() {
        System.out.println("Meow-meow!");
    }
}
