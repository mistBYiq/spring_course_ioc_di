package spring_introduction;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestValue {
    public static void main(String[] args) {
        ClassPathXmlApplicationContext context =
                new ClassPathXmlApplicationContext("applicationContext3.xml");

        /* Для внедрения строк и других значений можно использовать аннотацию @Value.
        В этом случае в сеттерах нет необходимости, как это было при конфигурации с помощью xml файла
        *
        Можно использовать проперти файл с нужными нам значениями
        */

        Person person = context.getBean("personBean", Person.class);
        person.callYourPet();

        System.out.println(person.getSurname());
        System.out.println(person.getAge());

        context.close();
    }
}
