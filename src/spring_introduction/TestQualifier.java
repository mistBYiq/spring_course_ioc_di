package spring_introduction;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestQualifier {
    public static void main(String[] args) {
        ClassPathXmlApplicationContext context =
                new ClassPathXmlApplicationContext("applicationContext3.xml");
        /* Как работает @Autowired: Спринг сканирует(ищет) классы помечанные @Component, затем видит @Autowired,
        и пытается внедрить зависимость - смотрит есть ли подходящие бины по типу, если находит, то внедряет зависимости,
        если нет - выбрасывается исключение, если больше 1 бина - выбрасывается исключение
        *
        если при использовании @Autowired, есть два подходящих бина, то используется @Qualifier
        в которой указывается какой конкретно бин должен быть внедрен
        *
        примеры использования:
        FIELD
        @Autowired
        @Qualifier("dog")
        private Pet pet;

        SETTER
        @Autowired
        @Qualifier("dog")
        public void setPet(Pet pet){this.pet=pet};

        CONSTRUCTOR
        @Autowired
        public Person(@Qualifier("dog") Pet pet){this.pet=pet}
        */

        Person person = context.getBean("personBean", Person.class);
        person.callYourPet();

        context.close();
    }
}
